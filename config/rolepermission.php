<?php

return [
    'name' => 'RolePermission',
    'roles' => [
      'admin' => 'Admin',
      'mentor head' => 'Mentor Head',
      'mentor' => 'Mentor',
      'merchant' => 'Merchant',
      'user' => 'User'
    ],
    'permissions' => [
      'administer rolepermission' => 'Administer Roles and Permissions'
    ]
];
