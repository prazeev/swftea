<?php

return [
    'name' => 'Chatroom',
    'permissions' => [
      'create room' => 'Create Room',
      'archive all room' => 'Archive Room',
      'join all room' => 'Join room',
      'kick any user from all room' => 'Kick any user in room',
      'lock all room' => 'Lock any room',
      'broadcast in any room' => 'broadcast in any room',
      'set max min amount in any bot room' => 'set max min amount in any bot room',
      'enter left secretly in room' => 'enter left secretly in room',
      'clear kicked user from all room' => 'clear kicked user from all room',
      'view block list of room' => 'view block list of room',
    ]
];
