<?php

return [
    'name' => 'RolePermission',
    'roles' => [
      'admin' => 'Admin',
      'mentor head' => 'Mentor Head',
      'mentor' => 'Mentor',
      'merchant' => 'Merchant',
      'user' => 'User',
      'global admin' => 'Global Admin',
      'legends' => 'Legends',
      'senior profile' => 'Senior Profile'
    ],
    'permissions' => [
      'administer rolepermission' => 'Administer Roles and Permissions'
    ],
];
