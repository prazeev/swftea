<?php

namespace Modules\Swfteacontest\Entities;

use Illuminate\Database\Eloquent\Model;

class SwfteaContestTermsAndContition extends Model
{
    protected $fillable = ['text'];
}
