<?php

return [
  'name' => 'AccountHistory',
  'permissions' => [
    'can send credit' => 'can send credit',
    'can receive credit' => 'can receive credit',
    'send credit to anyone' => 'send credit to anyone',
  ]
];
