<?php

return [
  'name' => 'ChatMini',
  'primary_command' => '/',
  'game_command' => '!',
  'packages' => [
    1 => [
      'title' => "Starter Pack",
      'description' => "This is basic pack for user.",
      'amount'=> 4555,
      'price'=> 99,
      'key' => 1,
      'image'=> null
    ],
    2 => [
      'title' => "Bronze Pack",
      'description' => "This pack is medium pack",
      'amount'=> 45555,
      'price'=> 999,
      'key' => 2,
      'image'=> null
    ],
    3 => [
      'title' => "Silver Pack (Best value)",
      'description' => "This pack is silver pack",
      'amount'=> 95555,
      'price'=> 1999,
      'key' => 3,
      'image'=> null
    ],
    4 => [
      'title' => "Gold Pack",
      'description' => "This pack is gold pack",
      'amount'=> 255555,
      'price'=> 4999,
      'key' => 4,
      'image'=> null
    ]
  ]
];
